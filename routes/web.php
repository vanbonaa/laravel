<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', 'HomeController@index');
// Route::get('/register', 'AuthController@register');
// Route::post('/welcome', 'AuthController@form');
Route::get('/master', function() {
    return view('admin.master');
});
Route::get('/table', function() {
    return view('items.table');
});
Route::get('/data-table', function() {
    return view('items.data-table');
});

Route::get('/jawaban/create', 'JawabanController@create');
Route::post('/jawaban', 'JawabanController@store');

Route::get('/pertanyaan/create', 'PertanyaanController@create');
Route::post('/pertanyaan', 'PertanyaanController@store');
Route::get('/pertanyaan', 'PertanyaanController@index');
Route::get('/pertanyaan/{id}', 'PertanyaanController@show');
Route::get('/pertanyaan/{id}/edit', 'PertanyaanController@edit');
Route::put('/pertanyaan/{id}', 'PertanyaanController@update');
Route::delete('/pertanyaan/{id}', 'PertanyaanController@destroy');