<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
        @csrf
        <label>First Name:</label> <br><br>
        <input type="text" name="namaawal" placeholder="First Name"> <br><br>
        <label>Last Name:</label> <br><br>
        <input type="text" name="namaakhir" placeholder="Last Name"><br><br>
        <label>Gender:</label> <br><br>
        <input type="radio" name="gen">Male<br>
        <input type="radio" name="gen">Female<br>
        <input type="radio" name="gen">Other<br><br>
        <label>Nationality:</label> <br><br>
        <select name="WN">
            <option value="indonesia">Indonesian</option>
            <option value="american">American</option>
            <option value="afrikan">Afrikan</option>
            <option value="europe">Europe</option>
        </select><br><br>
        <label>Language Spoken:</label> <br><br>
        <input type="checkbox" name="LS">Bahasa Indonesia<br>
        <input type="checkbox" name="LS">English<br>
        <input type="checkbox" name="LS">Other<br><br>
        <label>Bio:</label> <br><br>
        <textarea name="bio" cols="30" rows="10"></textarea>
        <br>
        <input type="submit" value="Sign Up">
    </form>
    
</body>
</html>