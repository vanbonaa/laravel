@extends('admin.master')

@section('content')
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Create New Jawaban</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/jawaban" method="POST">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="isi">Isi jawaban anda</label>
                    <input type="text" class="form-control" name="isi" placeholder="Enter jawaban">
                  </div>
                  <!-- <div class="form-group">
                    <label for="body">Body</label>
                    <input type="text" class="form-control" id="body" placeholder="Body">
                  </div> -->
                  
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Create</button>
                </div>
              </form>
            </div>
@endsection