<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('form');
    }
    public function form(Request $request)
    {
        $awal = $request->namaawal;
        $akhir = $request->namaakhir;
        // dd($awal,$akhir);

        return view('welcome', compact('awal', 'akhir'));
    }
}