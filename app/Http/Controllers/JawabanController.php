<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class JawabanController extends Controller
{
    public function create() {
        return view('jawaban.create');
    }

    public function store(Request $request) {
        // dd($request->all());
        $query = DB::table('jawaban')->insert([
            "isi" => $request["isi"]
        ]);

        return redirect('/jawaban/create');
    }
}
